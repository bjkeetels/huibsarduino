#include <Servo.h>
#define trigPin 12
#define echoPin 13

Servo myServo;
int pinRadarTotal = 5;          //Totaal aantal pinnen dat gebruikt wordt voor de radar ledjes
int pinsRadar[] = {1,2,3,4,5};  //Welke pinnen worden exact gebruikt voor de radar
int radarRounds = 6;            //Aantal keer dat de radar rond moet gaan            
int pinEye1= 6;                 //Welke pin wordt gebruikt voor het rechteroog
int pinEye2 = 7;                //Welke pin wordt gebruikt voor het linkeroog
int pinLaser1 = 8;              //Welke pin wordt gebruikt voor de laserstraal
int pos = 0;                    //Variable om de positie van de servo (hoofd) in graden
int val1 = 0;                   //Variable om tijdelijke waardes in op de slaan
int val2 = 0;                   //Variable om tijdelijke waardes in op de slaan


void setup() {
  for (int i=0; i<pinRadarTotal; i=i+1){    //Initialiseer alle pinnen van de radar als output (ledjes)
    pinMode(pinsRadar[i], OUTPUT);          
  }
  pinMode (pinEye1, OUTPUT);                //Initialiseer de pin voor het rechteroog als output (ledje)
  pinMode (pinEye2, OUTPUT);                //Initialiseer de pin voor het linkeroog als output (ledje)
  pinMode (pinLaser1, OUTPUT);              //Initialiseer de pin voor de laser als outpur (laser led)
  myServo.attach(9);                        //Initialiseer de servo motor van het hoofd
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop(){
  long duration, distance;
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;

  if (distance < 100) {
    digitalWrite(pinsRadar[1], HIGH);
  }
  else {
    digitalWrite(pinsRadar[1], LOW);  
  }
  if (distance < 80) {
    digitalWrite(pinsRadar[2], HIGH);
  }
  else {
    digitalWrite(pinsRadar[2], LOW);  
  }
  if (distance < 60) {
    digitalWrite(pinsRadar[3], HIGH);
  }
  else {
    digitalWrite(pinsRadar[3], LOW);  
  }
  if (distance < 40) {
    digitalWrite(pinsRadar[4], HIGH);
  }
  else {
    digitalWrite(pinsRadar[4], LOW);  
  }
  if (distance < 20) {
    digitalWrite(pinsRadar[5], HIGH);
    program1();  }
  else {
    digitalWrite(pinsRadar[5], LOW);  
  }
}

void program1() {
  setEye(HIGH, HIGH);
  digitalWrite(pinLaser1, HIGH);
  radarLoop(radarRounds);
  for (pos = 90; pos <= 180; pos += 1) {  // ga van 90 graden naar 180 graden
    myServo.write(pos);                   // vertel de servo de juiste positie in variable 'pos'
    delay(30);                            // wacht voor 30 ms voordat de servo van positie verranderd 
  }
  blinkEye(1); 
  for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myServo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(30);                       // waits 15ms for the servo to reach the position
  }
  blinkEye(1);
  for (pos = 0 ; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myServo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(30);                       // waits 15ms for the servo to reach the position
  }
  blinkEye(1);
  radarLoop(radarRounds);
  digitalWrite(pinLaser1, LOW);  
  delay(5000);
  setEye(LOW, LOW);
  //delay(5000);

}


void radarLoop(int x) {
  for (int i=0; i<x; i=i+1){
    for (int i=0; i<pinRadarTotal; i=i+1){
    digitalWrite(pinsRadar[i], HIGH);
    delay(100);
    digitalWrite(pinsRadar[i], LOW);
    }
  }
}
 
void blinkEye(int x){
  val1 = digitalRead(pinEye1);
  val2 = digitalRead(pinEye2);
  for (int i=0; i<x; i=i+1){
    setEye(HIGH, HIGH);
    delay(500);
    setEye(LOW, LOW);
    delay(500);
    setEye(val1, val2);
  }
}

void setEye(char(x), char(y)){
  digitalWrite(pinEye1, x);
  digitalWrite(pinEye2, y);
}
  
