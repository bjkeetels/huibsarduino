int pinsTotal = 8;
int pins[] = {2,3,4,5,6,7,8,9};
int wait = 10;

void setup () {
  for (int i=0; i<pinsTotal; i=i+1){
    pinMode(pins[i], OUTPUT);
  }
}

void loop() {
  wait=wait+100;
  for (int i=0; i<pinsTotal; i=i+1){
    digitalWrite(pins[i], HIGH);
    delay(wait);
    digitalWrite(pins[i], LOW);
    }
  for (int i=pinsTotal-1; i>0; i=i-1){
    digitalWrite(pins[i], HIGH);
    delay(wait);
    digitalWrite(pins[i], LOW);
    }
}
